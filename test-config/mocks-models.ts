import {
  IDivision, IGame, IMatch, IPool, IScores, ITeam, ITimeGroupListView, ITournament,
  IUser
} from '../src/models/models';

/**
 * Data Models
 */

export const UserMock: IUser = {
  '_id': '5a3c3cd5df26581300e3591f',
  'name': {
    'familyName': 'Warmanman',
    'givenName': 'Tom',
    'middleName': 'Albcejibcjiea'
  },
  'displayName': 'Tom Albcejibcjiea Warmanman',
  'firstName': 'Tom',
  'lastName': 'Warmanman',
  'gender': 'Male',
  'email': 'wiwoaddiek_1512941185@tfbnw.net',
  'image': 'https://graph.facebook.com/v2.6/122098395250468/picture?type=large',
  'facebookInfo': {
    'provider': 'facebook',
    'id': '122098395250468',
    'displayName': 'Tom Albcejibcjiea Warmanman',
    'name': {'familyName': 'Warmanman', 'givenName': 'Tom', 'middleName': 'Albcejibcjiea'},
    'gender': '',
    'emails': [{'value': 'wiwoaddiek_1512941185@tfbnw.net'}],
    'photos': [{'value': 'https://graph.facebook.com/v2.6/122098395250468/picture?type=large'}],
    '_json': {
      'id': '122098395250468',
      'name': 'Tom Albcejibcjiea Warmanman',
      'last_name': 'Warmanman',
      'first_name': 'Tom',
      'middle_name': 'Albcejibcjiea',
      'email': 'wiwoaddiek_1512941185@tfbnw.net'
    }
  },
  'memberships': [{
    '_id': '5a3c3cd5df26581300e3591e',
    'user': '5a3c3cd5df26581300e3591f',
    'provider': 'facebook',
    'providerUserId': '122098395250468',
    'accessToken': 'EAAEzu7qkw0UBAG3kXsRQrrNYZBRqwz1gi4OxEHSLn9dZAJKD9ykXw69oQSZBb7Yq1GdM47cHKN5jClTa5xtTTSNcvRIUr2ZCulbNVscx0hOWUDJLhNl9DdhUXETT81ZCZBnBmCq71DXMud57JTxFUsQci1I8qVktqueNhY79l9cSnWMoZCpVqVZBFbg5frYFIInkPYmpjs19ZAV3jzQepuEc3PRY5vQnMB50ZD',
    '__v': 0,
    'dateAdded': '2017-12-21T22:59:33.686Z'
  }],
  'sports': []
};

export const TeamMock: ITeam = {
  '_id': '5a38093a109d7b54af901bb0',
  'title': 'Dejnono',
  'tournaments': [
    '5a38093a109d7b54af901baa'
  ],
  'members': [
    UserMock
  ],
  'metadata': {
    '_id': '5a38093a109d7b54af901bb3',
    'tournament': '5a38093a109d7b54af901baa'
  }
};

export const PoolMock: IPool = {
  '_id': '5a38093a109d7b54af901bac',
  'title': '1',
  'division': '5a38093a109d7b54af901bab',
  'startTime': '2020-12-16T16:00:00.000Z',
  'matches': [
    '5a38093a109d7b54af901bc8',
    '5a38093a109d7b54af901bcc'
  ],
  'format': {
    'gameScores': [
      {
        'target': 25,
        'cap': 27,
        '_id': '5a38093a109d7b54af901baf'
      },
      {
        'target': 25,
        'cap': 27,
        '_id': '5a38093a109d7b54af901bae'
      },
      {
        'target': 15,
        'cap': null,
        '_id': '5a38093a109d7b54af901bad'
      }
    ]
  },
  'location': {
    'location': '',
    'court': 'A1'
  },
  'teams': [
    '5a38093a109d7b54af901bb0',
    '5a38093a109d7b54af901bb4'
  ]
};

export const MatchMock: IMatch = {
  _id: '',
  teams: [{_id: '', title: ''}],
  refTeam: {_id: '', title: ''},
  scheduledTime: new Date(),
  startTime: new Date(),
  location: {
    court: ''
  },
  games: []
};

export const GameMock: IGame = {
  _id: '',
  final: true,
  scores: []
};

export const ScoreMock: IScores = {
  timestamp: new Date(),
  score: [{
    team: '',
    points: 21
  }]
};

export const DivisionMock: IDivision = {
  '_id': '5a38093a109d7b54af901bab',
  'gender': 'Coed',
  'level': 'A',
  'pools': [
    PoolMock
  ],
  'teams': [
    TeamMock
  ]
};

export const TournamentMock: ITournament = {
  '_id': '5a38093a109d7b54af901baa',
  'title': 'Mini Tournament',
  'location': {
    'name': 'Serra Park',
    'city': 'Sunnyvale',
    'state': 'CA',
    'address': ''
  },
  'sport': 'Volleyball',
  'creator': UserMock,
  'status': 'DRAFT',
  'divisions': [
    DivisionMock
  ],
  'dates': [
    new Date('2017-12-18T08:00:00.000Z')
  ],
  'organizers': [],
  'image': ''
};


/**
 * View Models
 */

export const TimeGroupListViewMock: ITimeGroupListView = {
  present: {
    listItems: [],
    title: ''
  },
  future: {
    listItems: [],
    title: ''
  },
  past: {
    listItems: [],
    title: ''
  }
};
