import {Observable} from "rxjs/Observable";
import {IUser} from "../src/models/models";
import {IUserResponse} from "../src/providers/user-service";
import {TeamMock, TournamentMock, UserMock} from "./mocks-models";
import {Observer} from "rxjs/Observer";
import {LocalCacheCompoundKey, LocalCacheKeys} from "../src/providers/local-cache-service";

export const DataMocks = {
  loggedInResponse: {
    authResponse: {
      userID: UserMock.memberships[0].userID,
      accessToken: UserMock.memberships[0].accessToken
    },
    status: 'connected'
  },
  loggedOutResponse: {
    authResponse: {
      userID: '',
      accessToken: ''
    },
    status: 'unknown'
  }
};

export class SocialServiceMock {

  /**
   * Check to see if we are logged into FB. If Cordova, use Native. If not, use Web.
   * @returns {Promise<any>}
   */
  public checkFbLoginStatus(): any {
    return new Promise((resolve) => {
      resolve(DataMocks.loggedInResponse);
    })
  }

  fbLogin(): Promise<any> {
    return new Promise((resolve) => {
      resolve(DataMocks.loggedInResponse);
    })
  }

  fbLogout(): Promise<any> {
    return new Promise((resolve) => {
      resolve(DataMocks.loggedOutResponse);
    })
  }
}

export class UserServiceMock {

  public authenticateUser(accessToken: string): Observable<IUserResponse> {
    return Observable.of({
      user: UserMock,
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTNjM2NkNWRmMjY1ODEzMDBlMzU5MWYiLCJpYXQiOjE1MTM5MDAxMDd9.qz-Ty3VQeHPkDclgSN7EOwdY1pa9jZgGS9EjD2NW5_8'
    })
  }

  public getUser(): Observable<IUser> {
    return Observable.of(UserMock);
  }

  public setUser(user: IUser): Observable<any> {
    return Observable.of(UserMock);
  }

  public clearUser(): Observable<any> {
    return Observable.of(UserMock);
  }

}

export class DataServiceMock {
  getTournamentsInfo(): Observable<any[]> {
    return Observable.of([TournamentMock]);
  }
}

export class LocalCacheServiceMock {

  _cache = {
    "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTNjM2NkNWRmMjY1ODEzMDBlMzU5MWYiLCJpYXQiOjE1MTM5MDEwOTd9.rfAFmm1DX63PPvLZnjm5Qy6cAYpR60aLqBbr5hBMAgs",
    "Onboarding:5a3c3cd5df26581300e3591f": true,
    "Tournament:5a3c3cd5df26581300e3591f": TournamentMock,
    "Team:5a38093a109d7b54af901baa:5a3c3cd5df26581300e3591f": TeamMock
  };

  /**
   * Overloads _set and calls with the LocalCacheCompoundKey
   * @param {LocalCacheCompoundKey} compoundCacheKey
   * @param data
   * @param {boolean} useStorage
   * @returns {Observable<any>}
   */
  public setWithCompoundKey(compoundCacheKey: LocalCacheCompoundKey, data: any, useStorage: boolean = false): Observable<any> {
    return this._set(compoundCacheKey.key, data, useStorage);
  }

  /**
   * Overloads _set and calls with a LocalCacheKey
   * @param {LocalCacheKeys} cacheKey
   * @param data
   * @param {boolean} useStorage
   * @returns {Observable<any>}
   */
  public set(cacheKey: LocalCacheKeys, data: any, useStorage: boolean = false): Observable<any> {
    return this._set(LocalCacheKeys[cacheKey], data, useStorage);
  }

  /**
   * Sets a teamId in local storage. Once successful, we set runtime cache variable for easy promise/observable reso
   * @param {string} key
   * @param {any} data
   * @param {boolean} useStorage Toggle storing in local storage rather than just runtime cache
   * @returns {Observable<string>}
   */
  private _set(key: string, data: any, useStorage: boolean = false): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this._cache[key] = data;
      observer.next(data);
    });
  }

  /**
   * Overloads _get and calls with LocalCacheCompoundKey
   * @param {LocalCacheCompoundKey} compoundCacheKey
   * @returns {Observable<any>}
   */
  public getWithCompoundKey(compoundCacheKey: LocalCacheCompoundKey): Observable<any> {
    return this._get(compoundCacheKey.key);
  }

  /**
   * Overloads _get and calls with LocalCacheKey
   * @param {LocalCacheKeys} cacheKey
   * @returns {Observable<any>}
   */
  public get(cacheKey: LocalCacheKeys): Observable<any> {
    return this._get(LocalCacheKeys[cacheKey]);
  }

  /**
   * Gets a team Id from the app runtime cache variable. If cannot find that, it goes to local storage to fetch.
   * @returns {Observable<string>}
   */
  private _get(key: string): Observable<any> {
    return Observable.fromPromise(new Promise((resolve, reject) => resolve(this._cache[key]))
    );
  }

  /**
   * Overloads _clear and calls with LocalCompoundCacheKey
   * @param {LocalCacheCompoundKey} compoundCacheKey
   * @returns {Observable<any>}
   */
  public clearWithCompoundKey(compoundCacheKey: LocalCacheCompoundKey): Observable<any> {
    return this._clear(compoundCacheKey.key);
  }

  /**
   * Overloads _clear and calls with LocalCacheKey
   * @param {LocalCacheKeys} cacheKey
   * @returns {Observable<any>}
   */
  public clear(cacheKey: LocalCacheKeys): Observable<any> {
    return this._clear(LocalCacheKeys[cacheKey]);
  }

  /**
   * Clears the teamId in both local storage and runtime cache
   * @returns {Observable<string>}
   */
  private _clear(key: string): Observable<any> {
    this._cache[key] = null;
    return Observable.of();
  }
}
