import {async, TestBed} from '@angular/core/testing';
import {IonicErrorHandler, IonicModule, LoadingController, Platform} from 'ionic-angular';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {MyApp} from './app.component';
import {LoadingControllerMock, PlatformMock, SplashScreenMock, StatusBarMock} from '../../test-config/mocks-ionic';
import {TeamService} from "../providers/team-service";
import {TokenHelper, UserService} from "../providers/user-service";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {GlobalEventService} from "../providers/global-event-service";
import {StringHelpers} from "../helpers/string-helpers";
import {ErrorHandler} from "@angular/core";
import {RequestInterceptor} from "../helpers/request-interceptor";
import {DataService} from "../providers/data-service";
import {Facebook} from "@ionic-native/facebook";
import {SocialService} from "../providers/social-service";
import {LocalCacheService} from "../providers/local-cache-service";
import {TournamentService} from "../providers/tournament-service";
import {IonicStorageModule} from "@ionic/storage";
import {TeamPage} from "../pages/team/team";
import {MyTeamPage} from "../pages/team/my-team";
import {OnboardingPage} from "../pages/onboarding/onboarding";
import {TournamentPage} from "../pages/tournament/tournament";
import {GamePage} from "../pages/game/game";
import {PopoverPage} from "../pages/popovers/popover";
import {ErrorComponent} from "../components/error/error";
import {PoolPage} from "../pages/pool/pool";
import {NavbarComponent} from "../components/navbar/navbar";
import {DivisionsPage} from "../pages/divisions/divisions";
import {DateRangeDisplayPipe, DelimPipe, GetPipe, KeysPipe} from "../pipes/globals";
import {TeamSelectPage} from "../pages/team-select/team-select";
import {LoginPage} from "../pages/login/login";
import {TeamNotFoundComponent} from "../components/team-not-found/team-not-found";
import {AccountPage} from "../pages/account/account";
import {TabsPage} from "../pages/tabs/tabs";
import {MatchPage} from "../pages/match/match";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";
import {
  DataServiceMock, LocalCacheServiceMock, SocialServiceMock,
  UserServiceMock
} from "../../test-config/mocks-providers";

describe('MyApp Component', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MyApp,
        GamePage,
        PopoverPage,
        TournamentPage,
        TeamSelectPage,
        TeamPage,
        MyTeamPage,
        PoolPage,
        MatchPage,
        DivisionsPage,
        TeamNotFoundComponent,
        TabsPage,
        LoginPage,
        AccountPage,
        OnboardingPage,
        NavbarComponent,
        ErrorComponent,
        KeysPipe,
        DateRangeDisplayPipe,
        DelimPipe,
        GetPipe
      ],
      imports: [
        HttpClientModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
          name: '__scoreboardDb',
          driverOrder: ['indexeddb', 'sqlite', 'websql']
        })
      ],
      providers: [
        HttpClient,
        Facebook,
        TokenHelper,
        TeamService,
        GlobalEventService,
        TournamentService,
        StringHelpers,
        {provide: LocalCacheService, useClass: LocalCacheServiceMock},
        {provide: SocialService, useClass: SocialServiceMock},
        {provide: UserService, useClass: UserServiceMock},
        {provide: DataService, useClass: DataServiceMock},
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
        {provide: StatusBar, useClass: StatusBarMock},
        {provide: LoadingController, useClass: LoadingControllerMock},
        {provide: SplashScreen, useClass: SplashScreenMock},
        {provide: Platform, useClass: PlatformMock}
      ]
    }).overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [
          MyApp,
          GamePage,
          PopoverPage,
          TournamentPage,
          TeamSelectPage,
          TeamPage,
          MyTeamPage,
          PoolPage,
          MatchPage,
          DivisionsPage,
          TabsPage,
          LoginPage,
          AccountPage,
          OnboardingPage]
      }
    });
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MyApp);
    component = fixture.componentInstance;
    spyOn(component.nav, 'setRoot');
  }));

  it('should be created', () => {
    expect(component instanceof MyApp).toBe(true);
  });

  it('should navigate to the tournaments page', () => {
    expect(component.nav.setRoot).toHaveBeenCalledWith(TournamentPage, {}, {animate: true, direction: 'forward'});
  });

});
