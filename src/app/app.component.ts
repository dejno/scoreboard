import {Component, ViewChild} from '@angular/core';
import {MenuController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {TournamentPage} from '../pages/tournament/tournament';
import {TabsPage} from "../pages/tabs/tabs";
import {SocialService} from "../providers/social-service";
import {LoginPage} from "../pages/login/login";
import {TournamentService} from "../providers/tournament-service";
import {IUserResponse, UserService} from "../providers/user-service";
import {GlobalEventService, GlobalEventTopics} from "../providers/global-event-service";
import {Observable} from "rxjs/Observable";
import {AccountPage} from "../pages/account/account";
import {LocalCacheCompoundKey, LocalCacheKeys, LocalCacheService} from "../providers/local-cache-service";
import {ITeam, ITournament, IUser} from "../models/models";
import {OnboardingPage} from "../pages/onboarding/onboarding";
import {TeamService} from "../providers/team-service";
import {TeamSelectPage} from "../pages/team-select/team-select";
import {Storage} from "@ionic/storage";
import {Socket} from 'ng-socket-io';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public storage: Storage,
              private _menuController: MenuController,
              private _globalEventService: GlobalEventService,
              private _localCacheService: LocalCacheService,
              private _socialService: SocialService,
              private _socket: Socket,
              private _teamService: TeamService,
              private _tournamentService: TournamentService,
              private _userService: UserService) {

    platform.resume.subscribe(() => {
      this._socket.connect();
      this._setupDeepLinks();
    });

    Promise.all([this.platform.ready(), storage.ready()]).then(() => {
      this._socket.connect();

      // Setup deeplinking
      this._setupDeepLinks();

      // Listen for logout.
      this._globalEventService.subscribe(GlobalEventTopics.UserLogout, () => this._openLoginPage());
      this._globalEventService.subscribe(GlobalEventTopics.UserLogin, (user: IUser) => this._enterApp(user));

      this.login();

      this.statusBar.styleDefault();
    });
  }

  private _setupDeepLinks() {
    // only on devices
    if (!this.platform.is('cordova')) {
      return
    }
    const Branch = window['Branch'];
    Branch.initSession(data => {
      if (data['+clicked_branch_link']) {
        // read deep link data on click
        alert('Deep Link Data: ' + JSON.stringify(data));
      }
    });
  }

  private login() {
    // Check the login status and deal with this based on that.
    // TODO: get token and user stored in local storage on login instead of FB
    this._socialService.checkFbLoginStatus().then((res) => {
      if (res.status === 'connected') {
        // Good news. We are connected.
        this._userService.authenticateUser(res.authResponse.accessToken)
          .catch((err) => {
            this._openLoginPage();
            return Observable.throw(err);
          })
          .subscribe((response: IUserResponse) => {
            this._enterApp(response.user);
          });
      } else if (res.status === 'not_authorized') {
        // If the person is not logged into FB or the app does not have permissions.
        this._openLoginPage();
      } else {
        // What happens if there is no auth or available connection? Bring to login for now...
        this._openLoginPage();
      }
    }).catch((err) => {
      this._openLoginPage();
    });
  }

  /**
   * Enter the application. Should only be called once we are authenticated.
   * @private
   */
  private _enterApp(user: IUser) {
    let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Onboarding, user._id);
    this._localCacheService.getWithCompoundKey(compoundKey).subscribe((onboarding: boolean) => {

      if (!onboarding) {
        this.nav.setRoot(OnboardingPage, {
          user: user
        }, {animate: true, direction: 'forward'});
      } else {
        return this._tournamentService.syncLocalTournament()
          .flatMap((tournament: ITournament) => this._teamService.getCurrentUserTeamByTournament(tournament._id)
            .flatMap((team: ITeam) => this._teamService.setLocalTeam(team, tournament._id))
            .map((team: ITeam) => {
              return {
                tournament: tournament,
                team: team
              }
            }))
          .subscribe((results) => {
              if (!results.team) {
                this.nav.setRoot(TeamSelectPage, {
                  tournament: results.tournament
                }, {animate: true, direction: 'forward'});
              } else {
                this.nav.setRoot(TabsPage, {
                  tournament: results.tournament,
                  team: results.team
                }, {animate: true, direction: 'forward'});
              }
            },
            () => {
              this.nav.setRoot(TournamentPage, {}, {animate: true, direction: 'forward'});
            },
            () => this.splashScreen.hide()
          );
      }
    });
  }

  /**
   * Opens the tournaments page
   */
  public openTournamentPage() {
    this._openPage(TournamentPage);
  }

  /**
   * Go to account page
   */
  public openAccountPage() {
    this._openPage(AccountPage);
  }

  /**
   * Generic open page handler
   * @param page
   * @param navOptions
   * @private
   */
  private _openPage(page: any, navOptions: any = {animate: true, direction: 'forward'}) {
    this._menuController.close();
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (this.nav.getActive().component !== TabsPage) {
      this.nav.setRoot(page, navOptions);
    } else {
      this.nav.push(page, navOptions);
    }
  }

  /**
   * Navigates to the login page
   * @private
   */
  private _openLoginPage() {
    this.nav.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
  }
}
