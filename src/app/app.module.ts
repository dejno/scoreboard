import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicStorageModule} from '@ionic/storage';
//native
import {Facebook} from '@ionic-native/facebook';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {Deeplinks} from "@ionic-native/deeplinks";
//pages
import {MyApp} from './app.component';
import {GamePage} from '../pages/game/game';
import {PopoverPage} from '../pages/popovers/popover';
import {TournamentPage} from '../pages/tournament/tournament';
import {TeamPage} from '../pages/team/team';
import {DivisionsPage} from '../pages/divisions/divisions';
import {PoolPage} from '../pages/pool/pool';
import {MatchPage} from '../pages/match/match';
//pipes
import {DateRangeDisplayPipe, DelimPipe, GetPipe, KeysPipe} from '../pipes/globals';
//services
import {DataService} from '../providers/data-service';
import {LocalCacheService} from '../providers/local-cache-service';
//helpers
import {StringHelpers} from '../helpers/string-helpers';
import {SocketIoConfig, SocketIoModule} from 'ng-socket-io';
//components
import {NavbarComponent} from '../components/navbar/navbar';
import {TabsPage} from '../pages/tabs/tabs';
import {TeamService} from '../providers/team-service';
import {MyTeamPage} from '../pages/team/my-team';
import {TeamNotFoundComponent} from '../components/team-not-found/team-not-found';
import {ErrorComponent} from '../components/error/error';
import {SocialService} from '../providers/social-service';
import {LoginPage} from '../pages/login/login';
import {TournamentService} from '../providers/tournament-service';
import {TokenHelper, UserService} from '../providers/user-service';
import {GlobalEventService} from '../providers/global-event-service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {RequestInterceptor} from '../helpers/request-interceptor';
import {AccountPage} from '../pages/account/account';
import {OnboardingPage} from '../pages/onboarding/onboarding';
import {TeamSelectPage} from "../pages/team-select/team-select";
import {AppErrorHandler} from "../helpers/error-handler";
import {NavigationHelpers} from "../helpers/navigation-helpers";
import {ENV} from "@app/env";

@NgModule({
  declarations: [
    MyApp,
    GamePage,
    PopoverPage,
    TournamentPage,
    TeamSelectPage,
    TeamPage,
    MyTeamPage,
    PoolPage,
    MatchPage,
    DivisionsPage,
    TeamNotFoundComponent,
    TabsPage,
    LoginPage,
    AccountPage,
    OnboardingPage,
    NavbarComponent,
    ErrorComponent,
    KeysPipe,
    DateRangeDisplayPipe,
    DelimPipe,
    GetPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      platforms: {
        ios: {
          statusbarPadding: true
        }
      }
    }),
    IonicStorageModule.forRoot({
      name: '__scoreboardDb',
      driverOrder: ['sqlite', 'indexeddb', 'websql']
    }),
    SocketIoModule.forRoot(ENV.socketio as SocketIoConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    GamePage,
    PopoverPage,
    TournamentPage,
    TeamSelectPage,
    TeamPage,
    MyTeamPage,
    PoolPage,
    MatchPage,
    DivisionsPage,
    TabsPage,
    LoginPage,
    AccountPage,
    OnboardingPage
  ],
  providers: [
    Deeplinks,
    HttpClient,
    StatusBar,
    SplashScreen,
    Facebook,
    TokenHelper,
    DataService,
    TeamService,
    LocalCacheService,
    GlobalEventService,
    SocialService,
    TournamentService,
    UserService,
    NavigationHelpers,
    StringHelpers,
    IonicErrorHandler,
    {provide: ErrorHandler, useClass: AppErrorHandler},
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true}
  ]
})
export class AppModule {
}
