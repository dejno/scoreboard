export const ENV: any  = {
  mode: 'prod',
  api: 'https://tourneysports.co',
  facebook: {
    appid: '338356359971653',
    permissions: ['public_profile', 'user_friends', 'email']
  },
  socketio: {
    url: 'https://tourneysports.co',
    options: {}
  }
};
