export const ENV: any = {
  mode: 'dev',
  api: 'http://localhost:3000',
  facebook: {
    appid: '338356359971653',
    permissions: ['public_profile', 'user_friends', 'email']
  },
  socketio: {
    url: 'http://localhost:3000',
    options: {}
  }
};
