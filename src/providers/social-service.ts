import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

import {AlertController, Events, Platform} from 'ionic-angular';

import {ENV} from '@app/env';

import {Facebook} from '@ionic-native/facebook';
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GlobalEventService, GlobalEventTopics} from "./global-event-service";

declare const window: any;

@Injectable()
export class SocialService {

  isCordova: boolean;
  isIOS: boolean;
  isAndroid: boolean;

  constructor(public http: HttpClient,
              public events: Events,
              private _platform: Platform,
              private _facebook: Facebook,
              private _alertController: AlertController,
              private _globalEventService: GlobalEventService) {

    this.isAndroid = _platform.is('android');
    this.isCordova = _platform.is('cordova');
    this.isIOS = _platform.is('ios');

    this._platform.ready()
      .then(source => {

        // Subscribe to the logout trigger to ACTUALLY logout our user from facebook.
        this._globalEventService.subscribe(GlobalEventTopics.UserLogout, (this.fbLogout).bind(this));

        if (!this.isCordova) {
          if (!window.facebookConnectPlugin) {
            let alert = this._alertController.create({
              title: 'window.facebookConnectPlugin is not loaded for browser init.',
              buttons: ['Close']
            });
            alert.present();
            setTimeout(() => {
              window.facebookConnectPlugin.browserInit(ENV.facebook.appid);
            }, 2000);
          } else {
            window.facebookConnectPlugin.browserInit(ENV.facebook.appid);
          }
        }
      });
  }

  /**
   * Check to see if we are logged into FB. If Cordova, use Native. If not, use Web.
   * @returns {Promise<any>}
   */
  checkFbLoginStatus(): Promise<any> {
    if (this.isCordova) {
      return this._facebook.getLoginStatus();
    } else {
      return new Promise((resolve, reject) => {
        if (window.facebookConnectPlugin) {
          window.facebookConnectPlugin.getLoginStatus(resolve, reject);
        } else {
          let alert = this._alertController.create({
            title: 'window.facebookConnectPlugin is not loaded.',
            buttons: ['Close']
          });
          alert.present();
        }
      });
    }
  }

  /**
   * Login to Facebook.
   * Changes implementation
   * @returns {Promise<any>}
   */
  fbLogin(): Promise<any> {
    if (this.isCordova) {
      return this._facebook.getLoginStatus().then((res) => {
        if (res.status !== 'connected') {
          return this._facebook.login(ENV.facebook.permissions);
        } else {
          return res;
        }
      });
    } else {
      return new Promise((resolve, reject) => {
        window.facebookConnectPlugin.getLoginStatus(
          (res) => {
            if (res.status == 'connected') {
              resolve(res);
            } else {
              window.facebookConnectPlugin.login(ENV.facebook.permissions,
                res => {
                  resolve(res);
                },
                err => {
                  reject(err);
                })
            }
          },
          (err) => {
            reject(err);
          });
      });
    }
  }

  /**
   * Logout of facebook
   * @returns {Promise<any>}
   */
  fbLogout(): Promise<any> {
    return this.isCordova ? this._facebook.logout() : new Promise((resolve, reject) => {
      window.facebookConnectPlugin.logout(resolve, reject);
    });
  }
}
