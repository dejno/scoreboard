import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Subject} from "rxjs/Subject";
import {LocalCacheCompoundKey, LocalCacheKeys, LocalCacheService} from "./local-cache-service";
import {Observable} from "rxjs/Observable";
import {UserService} from "./user-service";
import {IMatch, IPool, ITeam, ITournament, IUser} from "../models/models";
import {DataService} from "./data-service";
import * as _ from 'lodash';
import {ReplaySubject} from "rxjs/ReplaySubject";
import {SOCKET_EVENTS} from "../helpers/socket-events";
import {Socket} from 'ng-socket-io';
import {TournamentService} from "./tournament-service";


/**
 * TeamService class to take care of all Team local cache/storage sets/gets/clears
 */
@Injectable()
export class TeamService {

  private _changeSubject: Subject<ITeam> = new Subject<ITeam>();
  private _removeSubject: Subject<ITeam> = new Subject<ITeam>();
  public getCurrentTeamSubject: ReplaySubject<ITeam> = new ReplaySubject<ITeam>(1);

  constructor(private _localCacheService: LocalCacheService,
              private _socket: Socket,
              private _tournamentService: TournamentService,
              private _userService: UserService,
              private _dataService: DataService) {

    // If there is a pool that is updated
    this._socket.on(SOCKET_EVENTS.POOL_UPDATED, (pool: IPool) => {
      this._tournamentService.getLocalTournament()
        .flatMap((tournament: ITournament) => tournament ? this.sync(tournament._id) : Observable.of(null));
    });
  }

  /**
   * Get the team for the tournament from the LocalCacheService and return the Observable
   * @returns {Observable<string>}
   */
  public getTeam(tournamentId: string): Observable<ITeam> {
    return this._userService.getUser()
      .flatMap((user) => {
        let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Team, tournamentId, user._id);
        return this._localCacheService.getWithCompoundKey(compoundKey);
      });
  }

  /**
   * Set the local team for the tournament you are viewing and update the Subject for component updates
   * @param {ITeam} team
   * @param {string} tournamentId
   * @returns {Observable<string>}
   */
  public setLocalTeam(team: ITeam, tournamentId: string): Observable<ITeam> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Team, tournamentId, user._id);
        return this._localCacheService.setWithCompoundKey(compoundKey, team, true);
      });
  }

  /**
   * Set the server team for the tournament you are viewing and update the Subject for component updates
   * @param {ITeam} team
   * @param {string} tournamentId
   * @returns {Observable<string>}
   */
  public setTeam(team: ITeam, tournamentId: string): Observable<ITeam> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        return this._dataService.addTeamMember(team._id, user._id)
          .map((team: ITeam) => {
            team.isTeam = true;
            return team;
          })
          .flatMap((team: ITeam) => this.setLocalTeam(team, tournamentId));
      })
      .do((team: ITeam) => this.getCurrentTeamSubject.next(team));
  }

  /**
   * Remove the local team for the tournament you are viewing from local storage and update the Subject
   * @param {ITeam} team
   * @param {string} tournamentId
   * @returns {Observable<any>}
   */
  public removeLocalTeam(team: ITeam, tournamentId: string): Observable<any> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Team, tournamentId, user._id);
        return this._localCacheService.clearWithCompoundKey(compoundKey);
      });
  }

  /**
   * Remove the server team for the tournament you are viewing from local storage and update the Subject
   * @param {ITeam} team
   * @param {string} tournamentId
   * @returns {Observable<any>}
   */
  public removeTeam(team: ITeam, tournamentId: string): Observable<any> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        return this._dataService.removeTeamMember(team._id, user._id)
          .flatMap((team: ITeam) => this.removeLocalTeam(team, tournamentId));
      })
      .do(() => this.getCurrentTeamSubject.next(null));
  }

  /**
   * Gets the team the user has selected or been assigned to in the tournament. Radical.
   * @param {string} tournamentId
   * @returns {Observable<ITeam>}
   */
  public getCurrentUserTeamByTournament(tournamentId: string): Observable<ITeam> {
    return this._userService.getUser()
      .flatMap((user: IUser) => this.getTeamByTournamentAndUser(tournamentId, user._id))
      .map((team: ITeam) => {
        team.isTeam = true;
        return team;
      })
  }

  /**
   * Gets the team for the user in the current tournament context. Valuable for the current
   * user usage in another method in dataService.
   * @param {string} tournamentId
   * @param {string} userId
   * @returns {Observable<ITeam>}
   */
  public getTeamByTournamentAndUser(tournamentId: string, userId: string): Observable<ITeam> {
    return this._dataService.getTeamByTournamentAndUser(tournamentId, userId);
  }

  /**
   * Gets the pool for the team
   * @param {string} teamId
   * @returns {Observable<IPool>}
   */
  public getTeamPool(teamId: string) {
    return this._dataService.getPoolByTeam(teamId)
      .map((pool: IPool) => {
        let matches = _.get(pool, 'matches', []);

        // Since this is a team, let's filter for just that team's matches, incl reffing
        if (pool) pool.matches = matches.filter((match: IMatch) => {
          let refTeamId = _.get(match, 'refTeam._id', '');
          let matchTeamIds = _.get(match, 'teams', []).map(team => team._id);
          return refTeamId === teamId || matchTeamIds.indexOf(teamId) > -1;
        });

        return pool;
      });
  }

  /**
   * Syncs the local cache with the server and replays on the observable to update
   * @param {string} tournamentId
   */
  public sync(tournamentId: string): Observable<ITeam> {
    return this.getCurrentUserTeamByTournament(tournamentId)
      .flatMap((team: ITeam) => this.setLocalTeam(team, tournamentId))
      .do((team: ITeam) => this.getCurrentTeamSubject.next(team))
      .catch((err) => {
        this.getCurrentTeamSubject.next(null);
        return Observable.throw(err);
      });
  }

  /**
   * Get all of the teams for the current user
   * @returns {Observable<ITeam[]>}
   */
  public getTeamsForCurrentUser(): Observable<ITeam[]> {
    return this._userService.getUser()
      .flatMap((user: IUser) => this._dataService.getTeamsByUserId(user._id));
  }

  /**
   * Returns the set subject so that components across the app can listen to when the team is updated. YYUUUGE.
   * @returns {Subject<string>}
   */
  get set() {
    return this._changeSubject;
  }

  /**
   * Returns the remove subject so that components across the app can listen to when team is removed. YYUUUGE AGAIN.
   * @returns {Subject<string>}
   */
  get remove() {
    return this._removeSubject;
  }

}
