import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {IUser} from "../models/models";
import {GlobalEventService, GlobalEventTopics} from "./global-event-service";
import {Observable} from "rxjs/Observable";
import {DataService} from "./data-service";
import {LocalCacheKeys, LocalCacheService} from "./local-cache-service";

/**
 * Allow the token to be outside of the user. This is so we don't have a circular dependency with the HTTP_INTERCEPTOR
 */
@Injectable()
export class TokenHelper {

  constructor(private _globalEventService: GlobalEventService,
              private _localCacheService: LocalCacheService) {
    // Listen for logout.
    this._globalEventService.subscribe(GlobalEventTopics.UserLogout, () => this.clearToken());
  }

  public getToken(): Observable<any> {
    return this._localCacheService.get(LocalCacheKeys.Token);
  }

  public setToken(token: string): Observable<any> {
    return this._localCacheService.set(LocalCacheKeys.Token, token, true);
  }

  public clearToken(): Observable<any> {
    return this._localCacheService.clear(LocalCacheKeys.Token);
  }
}

@Injectable()
export class UserService {

  public user: IUser;

  constructor(private _globalEventService: GlobalEventService,
              private _dataService: DataService,
              private _localCacheService: LocalCacheService,
              private _tokenHelper: TokenHelper) {
    // Listen for logout. Don't need to cleanup, because this is a service.
    this._globalEventService.subscribe(GlobalEventTopics.UserLogout, () => this.clearUser());
  }

  /**
   * Authenticate the user with the given access token from the social-service login
   * @param {string} accessToken
   * @returns {Observable<IUser>}
   */
  public authenticateUser(accessToken: string): Observable<IUserResponse> {
    return this._dataService.authenticateUser(accessToken)
      .flatMap((response: IUserResponse) => {
        return Observable.zip(
          this._tokenHelper.setToken(response.token),
          this.setUser(response.user)
        )
          .switchMap(() => Observable.of(response));
      });
  }

  public getUser(): Observable<IUser> {
    return this._localCacheService.get(LocalCacheKeys.User);
  }

  public setUser(user: IUser): Observable<any> {
    return this._localCacheService.set(LocalCacheKeys.User, user);
  }

  public clearUser(): Observable<any> {
    return this._localCacheService.clear(LocalCacheKeys.User);
  }
}

/**
 * Model for the new response from the auth endpoint
 */
export interface IUserResponse {
  user: IUser,
  token: string
}
