import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Storage} from '@ionic/storage';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observer} from "rxjs/Observer";
import {Pro} from '@ionic/pro';

export enum LocalCacheKeys {
  Onboarding,
  Team,
  Token,
  Tournament,
  User
}

/**
 * Class that wraps the functionality and type for a compound key
 * This is used primarily in the case where we are in a tournament
 */
export class LocalCacheCompoundKey {

  private _localCompoundCacheKey: string;
  private _delimiter: string = ':';

  /**
   * constructs a CompoundCacheKey with modifiers. Allows for lookup based on multiple modifiers (like tournament)
   * @param {LocalCacheKeys} cacheKey
   * @param {string} keyModfiers
   */
  constructor(cacheKey: LocalCacheKeys, ...keyModfiers: string[]) {
    if (keyModfiers.length <= 0) {
      throw 'Cannot create a compound key without key modifiers.';
    }

    keyModfiers.unshift(LocalCacheKeys[cacheKey]);
    this._localCompoundCacheKey = keyModfiers.join(this._delimiter); // Creates key:modifer:modifier... etc
  }

  /**
   * Simple getter for the key we create during construction of the compound key
   * @returns {string}
   */
  get key(): string {
    return this._localCompoundCacheKey;
  }
}

/**
 DataService Class: Currently only working with switch values, but will
 make this more generic.
 */
@Injectable()
export class LocalCacheService {

  private _cache: object = {};

  constructor(private _storage: Storage) {
  }

  /**
   * Overloads _set and calls with the LocalCacheCompoundKey
   * @param {LocalCacheCompoundKey} compoundCacheKey
   * @param data
   * @param {boolean} useStorage
   * @returns {Observable<any>}
   */
  public setWithCompoundKey(compoundCacheKey: LocalCacheCompoundKey, data: any, useStorage: boolean = false): Observable<any> {
    return this._set(compoundCacheKey.key, data, useStorage);
  }

  /**
   * Overloads _set and calls with a LocalCacheKey
   * @param {LocalCacheKeys} cacheKey
   * @param data
   * @param {boolean} useStorage
   * @returns {Observable<any>}
   */
  public set(cacheKey: LocalCacheKeys, data: any, useStorage: boolean = false): Observable<any> {
    return this._set(LocalCacheKeys[cacheKey], data, useStorage);
  }

  /**
   * Sets a teamId in local storage. Once successful, we set runtime cache variable for easy promise/observable reso
   * @param {string} key
   * @param {any} data
   * @param {boolean} useStorage Toggle storing in local storage rather than just runtime cache
   * @returns {Observable<string>}
   */
  private _set(key: string, data: any, useStorage: boolean = false): Observable<any> {
    if (useStorage) {
      return Observable.fromPromise(
        this._storage.set(key, data)
          .then(() => {
            Pro.getApp().monitoring.log(`Setting with key ${key} on local db: ${JSON.stringify(data)}`, { level: 'info' });
            this._cache[key] = data;
            return data;
          })
          .catch((err) => {
            Pro.getApp().monitoring.log(`Error setting ${JSON.stringify(data)} with key ${key} on local db: ${JSON.stringify(err)}`, { level: 'info' });
          })
      );
    } else {
      return Observable.create((observer: Observer<any>) => {
        this._cache[key] = data;
        observer.next(data);
      });
    }
  }

  /**
   * Overloads _get and calls with LocalCacheCompoundKey
   * @param {LocalCacheCompoundKey} compoundCacheKey
   * @returns {Observable<any>}
   */
  public getWithCompoundKey(compoundCacheKey: LocalCacheCompoundKey): Observable<any> {
    return this._get(compoundCacheKey.key);
  }

  /**
   * Overloads _get and calls with LocalCacheKey
   * @param {LocalCacheKeys} cacheKey
   * @returns {Observable<any>}
   */
  public get(cacheKey: LocalCacheKeys): Observable<any> {
    return this._get(LocalCacheKeys[cacheKey]);
  }

  /**
   * Gets a team Id from the app runtime cache variable. If cannot find that, it goes to local storage to fetch.
   * @returns {Observable<string>}
   */
  private _get(key: string): Observable<any> {
    return Observable.fromPromise(
      this._cache[key] ?
        new Promise((resolve, reject) => resolve(this._cache[key])) :
        this._storage.get(key)
          .then((data) => {
            Pro.getApp().monitoring.log(`Getting from local db with key ${key}: ${JSON.stringify(data)}`, { level: 'info' });
            this._cache[key] = data;
            return data;
          })
          .catch((err) => {
            Pro.getApp().monitoring.log(`Error getting data with key ${key} on local db: ${JSON.stringify(err)}`, { level: 'info' });
          })
    );
  }

  /**
   * Overloads _clear and calls with LocalCompoundCacheKey
   * @param {LocalCacheCompoundKey} compoundCacheKey
   * @returns {Observable<any>}
   */
  public clearWithCompoundKey(compoundCacheKey: LocalCacheCompoundKey): Observable<any> {
    return this._clear(compoundCacheKey.key);
  }

  /**
   * Overloads _clear and calls with LocalCacheKey
   * @param {LocalCacheKeys} cacheKey
   * @returns {Observable<any>}
   */
  public clear(cacheKey: LocalCacheKeys): Observable<any> {
    return this._clear(LocalCacheKeys[cacheKey]);
  }

  /**
   * Clears the teamId in both local storage and runtime cache
   * @returns {Observable<string>}
   */
  private _clear(key: string): Observable<any> {
    return Observable.fromPromise(
      this._storage.remove(key)
        .then(() => {
          this._cache[key] = null;
          return null;
        })
    );
  }

}
