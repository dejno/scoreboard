import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {LocalCacheCompoundKey, LocalCacheKeys, LocalCacheService} from "./local-cache-service";
import {ITournament, IUser} from "../models/models";
import {Observable} from "rxjs/Observable";
import {UserService} from "./user-service";
import {DataService} from "./data-service";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class TournamentService {

  public getCurrentTournamentSubject: ReplaySubject<ITournament> = new ReplaySubject<ITournament>(1);

  constructor(private _dataService: DataService,
              private _localCacheService: LocalCacheService,
              private _userService: UserService) {
  }

  /**
   * Set the local store tournament with a compound key for the user
   * @param tournament
   * @returns {Observable<any>}
   */
  public setLocalTournament(tournament: ITournament): Observable<ITournament> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Tournament, user._id);
        return this._localCacheService.setWithCompoundKey(compoundKey, tournament, true);
      });
  }

  /**
   * Returns the local tournament object by hitting the data service
   * @returns {Observable<ITournament>}
   */
  public getLocalTournament(): Observable<ITournament> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Tournament, user._id);
        return this._localCacheService.getWithCompoundKey(compoundKey);
      });
  }

  /**
   * Get Tournament by id
   * @param {string} id
   * @returns {Observable<ITournament>}
   */
  public getTournament(id: string): Observable<ITournament> {
    return this._dataService.getTournament(id);
  }

  /**
   * Gets the tournaments with teams that the current member is on
   * @returns {Observable<ITournament[]>}
   */
  public getTournamentsAndTeamsForCurrentUser(): Observable<ITournament[]> {
    return this._userService.getUser()
      .flatMap((user: IUser) => {
        return this._dataService.getTournamentsAndTeamsByUserId(user._id);
      });
  }

  /**
   * Syncs the local store tournament with the server
   * @returns {Observable<ITournament>}
   */
  public syncLocalTournament(): Observable<ITournament> {
    return this.getLocalTournament()
      .flatMap((tournament: ITournament) => {
        if (!tournament) return Observable.throw('Does not have a local tournament');
        return this.getTournament(tournament._id);
      })
      .flatMap((tournament: ITournament) => {
        if (!tournament) return Observable.throw('Does not have a network tournament');
        return this.setLocalTournament(tournament);
      })
      .do((tournament: ITournament) => this.getCurrentTournamentSubject.next(tournament));

  }

}
