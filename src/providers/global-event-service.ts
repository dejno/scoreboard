import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';

export enum GlobalEventTopics {
  TournamentSelected,
  UserLogout,
  UserLogin
}

@Injectable()
export class GlobalEventService {

  constructor(private _events: Events) {}

  /**
   * Subscribe to an event topic. Events that get posted to that topic will trigger the provided handler.
   *
   * @param {string} topic the topic to subscribe to
   * @param handlers
   */
  public subscribe(topic: GlobalEventTopics, ...handlers: Function[]): void {
    this._events.subscribe(GlobalEventTopics[topic], ...handlers);
  }
  /**
   * Unsubscribe from the given topic. Your handler will no longer receive events published to this topic.
   *
   * @param {string} topic the topic to unsubscribe from
   * @param {function} handler the event handler
   *
   * @return true if a handler was removed
   */
  public unsubscribe(topic: GlobalEventTopics, handler?: Function): boolean {
    return this._events.unsubscribe(GlobalEventTopics[topic], handler);
  }
  /**
   * Publish an event to the given topic.
   *
   * @param {string} topic the topic to publish to
   * @param args
   */
  public publish(topic: GlobalEventTopics, ...args: any[]): any[] {
    return this._events.publish(GlobalEventTopics[topic], ...args);
  }

}
