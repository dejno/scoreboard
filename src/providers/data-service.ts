import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {IDivision, IMatch, IPool, IScores, ITeam, ITournament} from '../models/models';
import {HttpClient} from "@angular/common/http";
import {IUserResponse} from "./user-service";

/**
 DataService Class: Currently only working with switch values, but will
 make this more generic.
 */
@Injectable()
export class DataService {

  constructor(private _http: HttpClient) {
  }

  authenticateUser(fbAccessToken: string): Observable<IUserResponse> {
    return this._postObject('/api/auth/facebook', {access_token: fbAccessToken});
  }

  //#region Web Service

  //#region Tournaments

  /**
   * GET all tournaments metadata (for use in list views)
   * @returns {Observable<any>}
   */
  getTournamentsInfo(): Observable<any[]> {
    return this._getJson('/api/tournaments/metadata');
  }

  /**
   * Get a single tournament based on tournament id.
   * @param tournamentId
   * @returns {Observable<any>}
   */
  getTournament(tournamentId: string): Observable<ITournament> {
    return this._getJson('/api/tournaments/' + tournamentId);
  }

  /**
   * Get a single tournament based on tournament id.
   * @param tournamentId
   * @returns {Observable<any>}
   */
  getTournamentFull(tournamentId: string): Observable<ITournament> {
    return this._getJson(`/api/tournaments/${tournamentId}/full`);
  }

  /**
   * Gets tournaments with teams nested by user id
   * @param {string} userId
   * @returns {Observable<ITournament[]>}
   */
  getTournamentsAndTeamsByUserId(userId: string): Observable<ITournament[]> {
    return this._getJson(`/api/tournaments/member/${userId}`);
  }

  //#endregion

  //#region Teams

  /**
   * Get all of the teams in a tournament from the API server.
   */
  getTeamsInTournament(tournamentId: string): Observable<ITeam[]> {
    return this._getJson('/api/teams/tournament/' + tournamentId);
  }

  /**
   * Get team from the API server.
   */
  getTeamById(teamId: string): Observable<ITeam> {
    return this._getJson('/api/teams/' + teamId);
  }

  /**
   * Gets a team by the tournamentId and the user. This is the main call used for viewing
   * 'my team' when in the tournament context.
   * @param {string} tournamentId
   * @param {string} userId
   * @returns {Observable<ITeam>}
   */
  getTeamByTournamentAndUser(tournamentId: string, userId: string): Observable<ITeam> {
    return this._getJson(`/api/teams/tournament/${tournamentId}/member/${userId}`);
  }

  /**
   * Returns a list of teams the user is a part of
   * @param {string} userId
   * @returns {Observable<ITeam[]>}
   */
  getTeamsByUserId(userId: string): Observable<ITeam[]> {
    return this._getJson(`/api/teams/member/${userId}`);
  }

  /**
   *
   * @param {string} teamId
   * @param {string} userId
   * @returns {Observable<ITeam>}
   */
  addTeamMember(teamId: string, userId: string): Observable<ITeam> {
    return this._patchObject(`/api/teams/${teamId}/member/${userId}/add`);
  }

  /**
   *
   * @param {string} teamId
   * @param {string} userId
   * @returns {Observable<ITeam>}
   */
  removeTeamMember(teamId: string, userId: string): Observable<ITeam> {
    return this._patchObject(`/api/teams/${teamId}/member/${userId}/remove`);
  }

  //#endregion

  //#region Pools

  /**
   * GET pool by id.
   * @param poolId
   * @returns {Observable<IPool>}
   */
  getPool(poolId: string): Observable<IPool> {
    return this._getJson('/api/pools/' + poolId)
  }

  /**
   * Gets Pools by division
   * @param {string} divisionId
   * @returns {Observable<any>}
   */
  getPoolsByDivision(divisionId: string): Observable<any> {
    return this._getJson('/api/pools/division/' + divisionId);
  }

  /**
   * Gets a single pool using the teamId
   * @param {string} teamId
   * @returns {Observable<any>}
   */
  getPoolByTeam(teamId: string): Observable<IPool> {
    return this._getJson('/api/pools/team/' + teamId);
  }

  //#endregion

  //#region Divisions

  /**
   * GET divisions from a tournament with Pools populated with Teams populated within Pool.
   * @param tournamentId
   */
  getDivisionsByTournament(tournamentId: string): Observable<IDivision[]> {
    return this._getJson('/api/divisions/tournament/' + tournamentId)
  }

  //#endregion

  //#region Matches

  /**
   * GET match by id.
   * @param {string} matchId
   * @returns {Observable<IMatch>}
   */
  getMatch(matchId: string): Observable<IMatch> {
    return this._getJson('/api/match/' + matchId);
  }

  /**
   * GET match by game id.
   * @param {string} gameId
   * @returns {Observable<IMatch>}
   */
  getMatchByGame(gameId: string): Observable<IMatch> {
    return this._getJson('/api/matches/game/' + gameId);
  }

  //#region Games


  /**
   * POST score to the server.
   * @param {string} gameId - ID of the game to post the score for.
   * @param {IScores} scores - An object containing timestamp and array of two scores.
   * @param {boolean}  final - flag to indicate we are saving the final score of not. {@default false}
   * @return {Observable<any>}
   */
  postScoresToGame(gameId: string, scores: IScores): Observable<any> {
    return this._postObject('/api/games/score',
      {
        gameId: gameId,
        score: scores
      })
  }

  /**
   * POST final score to game.
   * @param {string} gameId - Id of game to post final score.
   * @param {IScores} scores - final scores
   * @return {Observable<any>}
   */
  postFinalScoreToGame(gameId: string, scores: IScores): Observable<any> {
    return this._postObject('/api/games/score/final', {
      gameId: gameId,
      score: scores,
    })
  }

  /**
   * POST reset score to game.
   * @param {string} gameId - Id of game to post reset score to.
   * @return {Observable<any>}
   */
  postResetScoreToGame(gameId: string): Observable<any> {
    return this._postObject('/api/games/score/reset', {
      gameId: gameId
    })
  }

  //#endregion


  //#region Private Helpers

  /**
   * Reuse this for all json GET requests
   * TODO: Store in Local Storage and get from there if exists
   * TODO: Add the FB access token for request auth once we have server side changes in
   */
  private _getJson(url: string): Observable<any> {
    return this._http.get(url);
  }

  /**
   * Reuse this for all object POST requests
   * @param url
   * @param object Object Should be validated before calling this method.
   * @returns {Observable<any>} - json of object posted.
   * TODO: Store in Local Storage on POST response
   * TODO: Add the FB access token for request auth once we have server side changes in
   */
  private _postObject(url: string, object: Object = {}): Observable<any> {
    return this._http.post(url, object);
  }

  /**
   * Reuse this for all object PUT requests
   * @param url
   * @param object Object Should be validated before calling this method.
   * @returns {Observable<any>} - json of object posted.
   * TODO: Store in Local Storage on POST response
   * TODO: Add the FB access token for request auth once we have server side changes in
   */
  // private _putObject(url: string, object: Object = {}): Observable<any> {
  //   return this._http.put(url, object);
  // }

  /**
   * Reuse this for all object PATCH requests
   * @param url
   * @param object Object Should be validated before calling this method.
   * @returns {Observable<any>} - json of object posted.
   * TODO: Store in Local Storage on POST response
   * TODO: Add the FB access token for request auth once we have server side changes in
   */
  private _patchObject(url: string, object: Object = {}): Observable<any> {
    return this._http.patch(url, object);
  }

  //#endregion

}
