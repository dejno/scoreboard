import {Component} from '@angular/core';
import {Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {IGame, IMatch, IPool, IScores} from '../../models/models';
import {DataService} from '../../providers/data-service';
import {GamePage} from '../game/game';
import * as _ from 'lodash';
import {getMatchRecordString} from '../../lib/match';

@Component({
  selector: 'page-match',
  templateUrl: 'match.html',
})
export class MatchPage {

  public match: IMatch;
  public gameScores: Array<IScores>;
  public record: string;
  private _matchId: string;
  private _pool: IPool;
  private _loader: Loading;

  constructor(private _dataService: DataService,
              private _navController: NavController,
              private _loadingController: LoadingController,
              private _navParams: NavParams) {
    this._matchId = this._navParams.get('matchId');
    this._pool = this._navParams.get('pool');
    this._loader = this._loadingController.create();
    this._loader.present();
  }

  ionViewWillEnter() {
    this._dataService.getMatch(this._matchId).subscribe(
      data => {
        this.match = data;
        this.initiateViewModel(this.match);
      },
      err => {
        console.log('Failed to get match');
        this._loader.dismiss();
      },
      () => {
        this._loader.dismiss();
      }
    )
  }

  initiateViewModel(match: IMatch) {
    this.record = getMatchRecordString(match);
    this._setCurrentGame(match);
    this.gameScores = this._getCurrentGameScores(match);
  }

  /**
   * Select a game.
   * @param {IGame} game
   */
  selectGame(game: IGame) {
    this._navController.push(GamePage, {
      gameId: game['_id'],
      match: this.match
    });
  }

  //#region Private Methods

  private _getCurrentGameScores(match: IMatch): Array<IScores> {
    return _.map(match.games, (game: IGame) => _.last(game.scores))
  }

  private _setCurrentGame(match: IMatch) {
    if(!match) return;

    let foundCurr = false;
    _.each(match.games, game => {
      if (!foundCurr && !game.final) {
        game.isCurrent = true;
        foundCurr = true;
      }
    })
  }

  //#endregion

}
