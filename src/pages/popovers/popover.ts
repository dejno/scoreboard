import {Component} from '@angular/core';
import {AlertController, NavParams, ViewController} from 'ionic-angular';
import {Subject} from 'rxjs/Subject';


@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {

  private _popoverObservable: Subject<boolean>;

  constructor(private _alertController: AlertController,
              private _viewController: ViewController,
              private _navParams: NavParams) {
    this._popoverObservable = this._navParams.get('popoverSubject');
  }

  // On Reset show the alert dialog
  onReset() {
    this.showAlert();
  }

  /**
   Show alert for the reset
   */
  showAlert() {
    this._alertController.create({
      title: 'Reset the scores',
      message: 'Are you sure you want to reset the scores?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            this._popoverObservable.complete();
            this._viewController.dismiss()
          }
        },
        {
          text: 'Yes',
          handler: () => {
            if (this._popoverObservable instanceof Subject) {
              this._popoverObservable.next(true);
              this._popoverObservable.complete();
            }
            this._viewController.dismiss();
          }
        }
      ]
    }).present()
  }
}
