import {Component, ViewChild} from '@angular/core';
import {DataService} from '../../providers/data-service';
import {AlertController, LoadingController} from 'ionic-angular';
import {ITimeGroupListView, ITournament} from '../../models/models';
import {ENV} from '@app/env';
import {StringHelpers} from '../../helpers/string-helpers';
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {NavigationHelpers} from "../../helpers/navigation-helpers";

@Component({
  selector: 'tournament',
  templateUrl: 'tournament.html'
})
export class TournamentPage {
  @ViewChild('searchbar') searchbar: any;

  public tournamentListViewData: Subject<ITimeGroupListView> = new Subject<ITimeGroupListView>();
  private _cachedTournaments: Array<ITournament> = [];

  constructor(private _dataService: DataService,
              private _stringHelpers: StringHelpers,
              private _navigationHelpers: NavigationHelpers,
              private _loadingController: LoadingController,
              private _alertController: AlertController) {

    this._dataService.getTournamentsInfo()
      .catch(err => {
        console.log('Failed to get tournaments.');
        this._alertController.create({
          title: 'Debug',
          message: `Error: ${err.toString()} ENV: ${ENV.api}`,
          buttons: [
            {
              text: 'Ok',
              role: 'cancel'
            }
          ]
        }).present();

        return Observable.of(null);
      })
      .subscribe((data: Array<ITournament>) => {
        this._cachedTournaments = data;
        this.tournamentListViewData.next(this.createViewModel(this._cachedTournaments));
      });
  }

  /**
   * Creates a View Model for this page
   * TODO: Extract into a helper for other time based list views
   * @param {Array<ITournament>} data
   * @returns {ITimeGroupListView}
   */
  createViewModel(data: Array<ITournament>): ITimeGroupListView {
    let pastItems: Array<ITournament>;
    let presentItems: Array<ITournament>;
    let futureItems: Array<ITournament>;
    let today = new Date();

    // Reset the time to 0 so that we can make comparisons below
    today.setHours(0, 0, 0, 0);

    pastItems = data.filter(tournament => {
      return tournament.dates.every(date => {
        return normalizeDateFromString(date).getTime() < today.getTime();
      });
    });
    //TODO: This does not handle sparse dates.
    presentItems = data.filter(tournament => {
      return tournament.dates.some(date => {
        return normalizeDateFromString(date).getTime() == today.getTime();
      });
    });

    futureItems = data.filter(tournament => {
      return tournament.dates.every(date => {
        return normalizeDateFromString(date).getTime() > today.getTime();
      });
    });

    function normalizeDateFromString(date: Date) {
      let comparisonDate = new Date(date);
      comparisonDate.setHours(0, 0, 0, 0);
      return comparisonDate;
    }

    return {
      present: {
        listItems: presentItems,
        title: 'Current'
      },
      future: {
        listItems: futureItems,
        title: 'Future'
      },
      past: {
        listItems: pastItems,
        title: 'Past'
      }
    }
  }

  /**
   * Select a tournament a head to the teams page for those in the tournament
   */
  tournamentSelected(tournament: ITournament) {
    this._navigationHelpers.navigateToTournament(tournament);
  }

  /**
   * On search bar input, filter array of tournaments based on title.
   * @param event
   */
  onSearchInput() {
    let searchQuery = this.searchbar.value;
    if (searchQuery) {
      this.tournamentListViewData.next(this.createViewModel(this._cachedTournaments.filter(tournament => this._stringHelpers.testRegex(searchQuery, tournament.title))));
    } else {
      this.tournamentListViewData.next(this.createViewModel(this._cachedTournaments)); //TODO: Replace with another data
      // service call which will have
      // cached vals.
    }
  }

  /**
   * On search cancel, restore all options.
   * @param event
   */
  onSearchCancel() {
    this.tournamentListViewData.next(this.createViewModel(this._cachedTournaments));
  }
}
