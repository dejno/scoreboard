import {Component, ViewChild} from '@angular/core';
import {LoadingController, NavController, NavParams, Refresher} from 'ionic-angular';
import {IDivision, IPool, ITeam, ITournament} from '../../models/models';
import {DataService} from '../../providers/data-service';
import * as _ from 'lodash';
import {TeamPage} from '../team/team';
import {PoolPage} from '../pool/pool';
import {StringHelpers} from '../../helpers/string-helpers';
import {Observable} from 'rxjs/Observable';
import {TeamService} from '../../providers/team-service';
import {Subscription} from "rxjs/Subscription";

/**
 * Generated class for the DivisionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-divisions',
  templateUrl: 'divisions.html',
})
export class DivisionsPage {
  @ViewChild('searchbar') searchbar: any;

  //Tournament data
  private subscriptions: Array<Subscription> = [];
  public divisions: Array<IDivision>;
  public tournament: ITournament;
  private _cachedDivisions: Array<IDivision>;
  private myTeam: string;

  constructor(private _dataService: DataService,
              private _teamService: TeamService,
              private _stringHelpers: StringHelpers,
              private _navController: NavController,
              private _navParams: NavParams,
              private _loadingController: LoadingController) {
    let loader = this._loadingController.create();
    this.tournament = this._navParams.get('tournament');

    loader.present();
    this.loadTournament(this.tournament._id)
      .then(tournament => loader.dismiss())
      .catch(err => loader.dismiss()) //TODO: Handle error gracefully

    this.subscriptions.push(this._teamService.getCurrentTeamSubject.subscribe((team: ITeam) => {
      if (team) this.myTeam = team._id // need to check for null here. Going to send 'null' if we remove current team.
    }));
  }

  //#region Public Methods

  /**
   * Reload tournament data from pull to refresh.
   * @param {Refresher} refresher
   */
  public reloadTournament(refresher: Refresher) {
    this.loadTournament(this.tournament._id)
      .then(tournament => refresher.complete())
      .catch(err => refresher.cancel());
  }

  /**
   * Load the tournament data again. (Not really necessary right now since no dynamic data, but can be built out)
   * @param tournamentId -  id of the tournament to load.
   * @returns {Promise<ITournament>}
   */
  public loadTournament(tournamentId: string): Promise<Array<ITournament | Array<IDivision> | ITeam>> {
    return new Promise((resolve, reject) => {
      Observable.zip(
        this._dataService.getTournament(tournamentId),
        this._dataService.getDivisionsByTournament(tournamentId)
      ).subscribe(
        (results) => {
          this.tournament = results[0];
          this.divisions = results[1];
          this._cachedDivisions = results[1];
          resolve(results);
        },
        err => {
          reject(err);
          console.log('Failed to get tournament and divisions.');
        },
        () => {
        }
      )
    })

  }

  //#endregion

  //#region Item Selections

  /**
   * Select a tournament a head to the teams page for those in the tournament
   */
  teamSelected(team: ITeam) {
    if (team._id === this.myTeam) {
      //if this is our selected team, select the 'Team' tab.
      this._navController.parent.select(0);
    } else {
      this._navController.push(TeamPage, {
        teamId: team._id,
        tournament: this.tournament
      });
    }

  }

  /**
   * Select a pool from the list of divisions.
   */
  poolSelected(pool: IPool) {
    this._navController.push(PoolPage, {
      poolId: pool._id,
      tournament: this.tournament
    });
  }

  //#endregion


  //#region Search Bar

  /**
   * On search bar input, filter array of divisions, pools, teams based on team title.
   * @param event
   */
  onSearchInput(event) {
    this.divisions = _.cloneDeep(this._cachedDivisions);
    let searchQuery: string = this.searchbar.value;
    if (this.divisions && searchQuery) {
      this.divisions = this.divisions.filter((division: IDivision) => {
        division.pools = division.pools.filter((pool: IPool) => {
          pool.teams = pool.teams.filter((team: ITeam) => this._stringHelpers.testRegex(searchQuery, team.title));
          return (pool.teams.length > 0)
        });
        return (division.pools.length > 0);
      })
    }
  }

  /**
   * On search cancel, restore all options.
   * @param event
   */
  onSearchCancel() { //TODO: Replace with another data service call which will have cached vals
    this.divisions = _.cloneDeep(this._cachedDivisions);
  }

  /**
   * Lifecycle event to help cleanup subscriptions to the team service when the view unloads
   */
  ionViewWillUnload() {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  //#endregion

}
