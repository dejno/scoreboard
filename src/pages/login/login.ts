import {Component} from "@angular/core";
import {SocialService} from "../../providers/social-service";
import {TournamentPage} from "../tournament/tournament";
import {AlertController, NavController} from "ionic-angular";
import {TournamentService} from "../../providers/tournament-service";
import {TabsPage} from "../tabs/tabs";
import {IUserResponse, UserService} from "../../providers/user-service";
import {Observable} from "rxjs/Observable";
import {ITournament} from "../../models/models";
import {TeamService} from "../../providers/team-service";
import {GlobalEventService, GlobalEventTopics} from "../../providers/global-event-service";

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(private _alertController: AlertController,
              private _globalEventService: GlobalEventService,
              private _socialService: SocialService,
              private _navController: NavController,
              private _teamService: TeamService,
              private _tournamentService: TournamentService,
              private _userService: UserService) {
  }

  /**
   * Click handler for the login button.
   */
  login() {
    this._socialService.fbLogin()
      .then(res => {
        this._userService.authenticateUser(res.authResponse.accessToken)
          .subscribe((userResponse: IUserResponse) => {
            // Login was a success! Let's enter the application.
            this._globalEventService.publish(GlobalEventTopics.UserLogin, userResponse.user);
          });
      })
      .catch(err => {
        // Login failed... let's tell the user.
        let alert = this._alertController.create({
          title: 'Login Failed',
          subTitle: 'Your login attempt to Facebook failed.',
          buttons: ['Close']
        });
        alert.present();
      });
  }
}
