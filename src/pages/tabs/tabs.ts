import {Component} from "@angular/core";
import {DivisionsPage} from "../divisions/divisions";
import {NavParams} from "ionic-angular";
import {MyTeamPage} from "../team/my-team";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: Object = MyTeamPage;
  tab2Root: Object = DivisionsPage;

  tabData: any = {};

  constructor(private _navParams: NavParams) {
    let tournament = _navParams.get('tournament');

    if(!tournament) {
      throw 'Tabs require tournament';
    }

    this.tabData = this._navParams.data;
  }
}
