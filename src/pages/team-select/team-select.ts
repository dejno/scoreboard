import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {IDivision, ITeam, ITournament} from "../../models/models";
import {TeamService} from "../../providers/team-service";
import {DataService} from "../../providers/data-service";
import {Subject} from "rxjs/Subject";
import {StringHelpers} from "../../helpers/string-helpers";
import * as _ from 'lodash';
import {TabsPage} from "../tabs/tabs";


@Component({
  selector: 'team-select',
  templateUrl: 'team-select.html'
})
export class TeamSelectPage {

  @ViewChild('searchbar') searchbar: any;

  private _tournament: ITournament;
  private _divisions: Subject<IDivision[]> = new Subject<IDivision[]>();
  private _cachedDivisions: IDivision[];


  constructor(private _navController: NavController,
              private _dataService: DataService,
              private _teamService: TeamService,
              private _navParams: NavParams,
              private _stringHelpers: StringHelpers) {

    this._tournament = this._navParams.get('tournament');

    this._dataService.getDivisionsByTournament(this._tournament._id).subscribe((divisions) => {
      this._cachedDivisions = divisions.map((division: IDivision) => {
        // sort Alphabetically
        division.teams = _.orderBy(division.teams, ['title'], ['asc']);
        return division;
      });

      this._divisions.next(this._cachedDivisions);
    });
  }

  /**
   * Select a team
   */
  teamSelected(team: ITeam) {
    this._teamService.setTeam(team, this._tournament._id)
      .subscribe((team: ITeam) => {
          // using the [''] selector here because the IDE was complaining...
          if (this._navController['root']) {
            // Pop to the view if we already have it
            this._navController.pop();
          } else {
            // If not, let's make a new one and push
            this._navController.push(TabsPage, {
              teamId: team._id,
              tournament: this._tournament
            });
          }

        },
        err => {
          console.log('Failed to set local team.', JSON.stringify(err));
        });
  }

  /**
   * The user has decided to choose a team later
   */
  selectLater() {
    this._navController.push(TabsPage, {
      tournament: this._tournament,
      teamNotFound: true
    });
  }

  /**
   * On search bar input, filter array of divisions, pools, teams based on team title.
   * @param event
   */
  onSearchInput() {
    let divisions = _.cloneDeep(this._cachedDivisions);
    let searchQuery: string = this.searchbar.value;
    if (divisions && searchQuery) {
      this._divisions.next(divisions.map((division: IDivision) => {
        division.teams = division.teams.filter((team: ITeam) => {
          return this._stringHelpers.testRegex(searchQuery, team.title);
        });
        return division;
      }));
    } else {
      this.onSearchCancel();
    }
  }

  /**
   * On search cancel, restore all options.
   * @param event
   */
  onSearchCancel() {
    this._divisions.next(_.cloneDeep(this._cachedDivisions));
  }

}
