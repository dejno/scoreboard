import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TournamentPage} from "../tournament/tournament";
import {LocalCacheCompoundKey, LocalCacheKeys, LocalCacheService} from "../../providers/local-cache-service";
import {IUser} from "../../models/models";

@Component({
  selector: 'onboarding',
  templateUrl: 'onboarding.html'
})
export class OnboardingPage {

  private _user: IUser;

  constructor(private _navController: NavController,
              private _localCacheService: LocalCacheService,
              private _navParams: NavParams) {
    this._user = this._navParams.get('user');
  }

  /**
   * Done with onbaording, time to select a tournament
   */
  goToHome() {
    let compoundKey = new LocalCacheCompoundKey(LocalCacheKeys.Onboarding, this._user._id);
    this._localCacheService.setWithCompoundKey(compoundKey, true, true).subscribe(() => {
      this._navController.setRoot(TournamentPage, {}, {animate: true, direction: 'forward'})
    });
  }

}
