import {Component} from '@angular/core';
import {AlertOptions, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import {PopoverPage} from '../popovers/popover';
import {IMatch, IScores} from '../../models/models';
import * as _ from 'lodash';
import {DataService} from '../../providers/data-service';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {ITeamScoreViewModelMap, getGameWinner, getTeamScoreViewModelMapFromGame} from '../../lib/game';

@Component({
  selector: 'page-game',
  templateUrl: 'game.html'
})
export class GamePage {

  //#region public view members
  public popover;
  public teamScoreViewModelMap: ITeamScoreViewModelMap;
  public teams: Array<string>;
  public selectOptionPoints: Array<number>;
  public selectOptionsMap: { [team: string]: AlertOptions };
  public isFinal: boolean;
  public changedFinal: boolean;
  //#endregion

  //#region private members
  private _popoverScoreSubject: Subject<boolean>;
  private _match: IMatch;
  private _gameId: string;
  private _initialScore: ITeamScoreViewModelMap;  //for comparison is final score is change
  //#endregion

  constructor(private _alertCtrl: AlertController,
              private _navParams: NavParams,
              private _dataService: DataService,
              private _toastCtrl: ToastController,
              private _popoverCtrl: PopoverController) {
    this._match = this._navParams.get('match');
    this._gameId = this._navParams.get('gameId');
    this._instantiateViewModels(this._gameId, this._match);
  }

  /**
   * Instantiate the necessary view models.
   * @param {string} gameId - id of game.
   * @param {IMatch} match - match containing this game.
   * @private
   */
  private _instantiateViewModels(gameId: string, match: IMatch) {
    let game = this._match.games.find(game => game['_id'] === gameId);
    this.teams = _.map(match.teams, team => team._id);
    this.teamScoreViewModelMap = getTeamScoreViewModelMapFromGame(game, match.teams);
    this.isFinal = game.final;
    this._initialScore = _.cloneDeep(this.teamScoreViewModelMap);
    this.selectOptionPoints = _.range(0, 50);
    this.selectOptionsMap = this._instantiateSelectOptions(this.teams);
  }

  /**
   * Instantiate score select options mapped by teamId.
   * @param {Array<string>} teams - array of team ids.
   * @return [team: string]: AlertOptions
   * @private
   */
  private _instantiateSelectOptions(teams: Array<string>): { [team: string]: AlertOptions } {
    let options = {};
    _.each(teams, teamId => {
      options[teamId] = {
        title: _.get(this.teamScoreViewModelMap[teamId], 'title', 'Set Score'),
        cssClass: 'select-score-popover'
      }
    });
    return options;
  }

  //#region scoring

  /**
   * Increase score by 1.
   * @param {string} teamId
   */
  public increaseScore(teamId: string) {
    this.teamScoreViewModelMap[teamId]['points']++;
    this._updateScore()
  }

  /**
   * Decrease score by 1
   * @param {string} teamId
   */
  public decreaseScore(teamId: string) {
    if (this.teamScoreViewModelMap[teamId]['points'] > 0) {
      this.teamScoreViewModelMap[teamId]['points']--;
      this._updateScore()
    }
  }

  public selectChange() {
    this._updateScore();
  }

  /**
   * On set final score, present an alert for confirming and call api to post to server.
   */
  public onSetFinalScore() {
    let keys = _.keys(this.teamScoreViewModelMap);
    let teamOne = this.teamScoreViewModelMap[keys[0]];
    let teamTwo = this.teamScoreViewModelMap[keys[1]];
    this._alertCtrl.create({
      title: 'Set Final Score',
      message: `
        <p ion-text>${teamOne.title}: <a ion-text>${teamOne.points}</a></p>
        <p ion-text>${teamTwo.title}: <a ion-text>${teamTwo.points}</a></p>
      `,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Confirm',
          handler: () => {
            this._postFinalScore().subscribe(
              () => this._presentFinalScoreToast()
            )
          }
        }
      ]
    }).present()
  }

  /**
   * Reverse the order of our teams array. (aka. switch sides!!)
   */
  public onSwitchSides() {
    _.reverse(this.teams);
  }

  //#endregion

  //#region Toasts and Popovers

  /**
   Show the popover settings to the user
   */
  public presentPopover(ev) {
    this._popoverScoreSubject = new Subject<boolean>();
    let popoverSubscription = this._popoverScoreSubject.subscribe(
      resetScore => {
        if (resetScore) this._resetScore()
      },
      err => {},
      () => popoverSubscription.unsubscribe()
    );
    this._popoverCtrl
      .create(PopoverPage, {popoverSubject: this._popoverScoreSubject})
      .present({
        ev: ev
      })
  }

  /**
   * Present final score toast.
   * @return {Promise<any>}
   * @private
   */
  private _presentFinalScoreToast(): Promise<any> {
    return this._toastCtrl.create({
      position: 'top',
      duration: 2000,
      message: this._getFinalScoreToastMessage()
    }).present();
  }

  /**
   * Get the message to use in final score toast display based on winner of game.
   * @return {string}
   * @private
   */
  private _getFinalScoreToastMessage(): string {
    let winnerId = getGameWinner(this.teamScoreViewModelMap);
    if (winnerId) {
      let loserId = _.filter(_.keys(this.teamScoreViewModelMap), key => key !== winnerId);
      return `${this.teamScoreViewModelMap[winnerId].title} won that one ${this.teamScoreViewModelMap[winnerId].points} to ${this.teamScoreViewModelMap[loserId].points}.`
    } else {
      return `Tied. ${this.teamScoreViewModelMap[this.teams[0]].points} to ${this.teamScoreViewModelMap[this.teams[1]].points}`
    }
  }

  //#endregion

  //#region Private Methods

  /**
   * Push the updated score to the DB.
   * final {boolean} - flag to set final score for score update.
   * @private
   */
  private _updateScore(final: boolean = false): Observable<any> {

    if (final) {
      this.isFinal = true;
      this._initialScore = _.cloneDeep(this.teamScoreViewModelMap);
    }

    if (this.isFinal) {
      this.changedFinal = this._finalScoreChanged(this.teamScoreViewModelMap);
    }

    return Observable.fromPromise(new Promise((resolve, reject) => {
        if (final) {
          this._dataService.postFinalScoreToGame(this._gameId, this._getIScoreFromTeamScoreViewModels(this.teamScoreViewModelMap))
            .subscribe(
              data => resolve(data),
              err => reject(err)
            )
        } else if (!this.isFinal) {
          this._dataService.postScoresToGame(this._gameId, this._getIScoreFromTeamScoreViewModels(this.teamScoreViewModelMap))
            .subscribe(
              data => {
                console.log('Score Updated.');
                resolve(data);
              },
              err => {
                console.log('Error updating scoring.');
                reject(err);
              }
            )
        } else {
          resolve();
        }
      })
    )


  }

  /**
   * Send final score for this game to the server.
   * @desc uses the score cached in teamScoreViewModelMap as source of truth.
   * @private
   */
  private _postFinalScore() {
    return this._updateScore(true)
  }

  /**
   * Reset the score to 0.
   * Sets this.isFinal to false.
   * @private
   */
  private _resetScore() {
    let keys = _.keys(this.teamScoreViewModelMap);
    _.each(keys, key => {
      this.teamScoreViewModelMap[key].points = 0;
    });
    this.changedFinal = false;
    this.isFinal = false;
    this._dataService.postResetScoreToGame(this._gameId).subscribe(
      data => {
        console.log('Reset score.')
      },
      err => console.log('Failed to reset score.')
    )
  }

  /**
   * Check current score against the initial score final score
   * @param {ITeamScoreViewModelMap} teamScores - current score to check against.
   * @return {boolean}
   * @private
   */
  private _finalScoreChanged(teamScores: ITeamScoreViewModelMap): boolean {
    if (!this.isFinal) {
      return false;
    }

    for (let key in teamScores) {
      if (teamScores.hasOwnProperty(key)) {
        if (teamScores[key].points !== _.get(this._initialScore[key], 'points')) {
          return true;
        }
      }
    }
    return false;
  }


  /**
   * Transform ITeamScoreViewModel to IScores to send to DB.
   * @param {ITeamScoreViewModelMap} teamScores
   * @returns {IScores}
   * @private
   */
  private _getIScoreFromTeamScoreViewModels(teamScores: ITeamScoreViewModelMap): IScores {
    return {
      score: _.map(teamScores, (teamScore) => {
        return {team: teamScore['team'], points: teamScore['points']}
      })
    };
  }

//#endregion


}
