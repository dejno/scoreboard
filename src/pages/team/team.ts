import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams, Refresher} from 'ionic-angular';
import {DataService} from '../../providers/data-service';
import {IPool, ITeam, ITournament} from '../../models/models';
import {Observable} from 'rxjs/Observable';
import {TeamService} from '../../providers/team-service';
import {Subscription} from 'rxjs/Subscription';
import {PoolPage} from '../pool/pool';
import {MatchPage} from '../match/match';
import {TeamSelectPage} from "../team-select/team-select";

@Component({
  selector: 'team',
  templateUrl: 'team.html'
})
export class TeamPage {

  // List of team within a team
  protected team: ITeam;
  protected tournament: ITournament;
  protected subscriptions: Array<Subscription> = [];
  private pool: Observable<IPool>;

  constructor(private _dataService: DataService,
              private _navParams: NavParams,
              private _loadingController: LoadingController,
              private _navController: NavController,
              protected _teamService: TeamService) {
    let teamId = this._navParams.get('teamId');
    this.tournament = this._navParams.get('tournament');

    // Subscribe to clearing the team
    this.subscriptions.push(this._teamService.remove.subscribe(() => {
      if (this.team) this.team.isTeam = false;
    }));

    // Subscribe to setting the team
    this.subscriptions.push(this._teamService.set.subscribe((team: ITeam) => {
      if (this.team) this.team.isTeam = this.team._id === team._id;
    }));

    // If there is a teamId, let's init the team!
    if (teamId) this.initTeam(teamId);

  }

  /**
   * Lifecycle event to help cleanup subscriptions to the team service when the view unloads
   */
  ionViewWillUnload() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Initialize the team for this view
   * @param {string} teamId
   * @param {boolean} showLoader
   */
  protected initTeam(teamId: string, showLoader: boolean = true): Promise<boolean> {
    let loader = this._loadingController.create();
    let getLocalTeamObservable = this._teamService.getTeam(this.tournament._id);
    let getTeamObservable = this._dataService.getTeamById(teamId);

    if (showLoader) loader.present();

    // Using async pipe observable pattern here to render this out whenever we have it.
    this.pool = this._teamService.getTeamPool(teamId);

    return new Promise((resolve, reject) => {
      Observable.zip(
        getTeamObservable,
        getLocalTeamObservable
      )
        .subscribe(results => {
            this.team = results[0];
            if (this.team) this.team.isTeam = this.team._id === results[1]._id;
            resolve(true);
          },
          err => {
            console.log('Failed to get team.');
            loader.dismiss();
            reject(false)
          },
          () => {
            loader.dismiss();
          }
        );
    })
  }


  /**
   * Reload team data from pull to refresh.
   * @param {Refresher} refresher
   */
  public reloadTeam(refresher: Refresher) {
    if (!this.team)
      return refresher.cancel();

    this.initTeam(this.team._id, false)
      .then(() => refresher.complete())
      .catch(() => refresher.cancel())

  }

  /**
   * Sets the current local team, and changes the toggle ui
   * Removes the current team from local and server
   * @param {ITeam} team
   */
  setTeam(team: ITeam) {
    this._teamService.getCurrentTeamSubject
      .first() // Only listen once
      .flatMap((previousTeam: ITeam) => {
        return previousTeam ? this._teamService.removeTeam(previousTeam, this.tournament._id) : Observable.of();
      })
      .flatMap(() => this._teamService.setTeam(team, this.tournament._id))
      .subscribe((newTeam: ITeam) => {
          this.team.isTeam = this.team._id === newTeam._id;
        },
        err => {
          console.log('Failed to set local team.', err);
        });
  }

  /**
   * Removes the current local team from storage, and change the toggle ui
   */
  removeTeam(team: ITeam) {
    this._teamService.removeTeam(team, this.tournament._id)
      .subscribe(() => {
        if (this.team) this.team.isTeam = false
      });
  }

  /**
   * Select a match and navigate to the view
   * @param {string} matchId
   */
  matchSelected(matchId: string) {
    this._navController.push(MatchPage, {
      matchId: matchId
    });
  }

  /**
   * Select a pool and navigate to the view
   * @param {string} poolId
   */
  poolSelected(poolId: string) {
    this._navController.push(PoolPage, {
      poolId: poolId,
      tournament: this.tournament
    });
  }

  /**
   * Callback for selectTeam button on team-not-found page
   */
  selectTeam() {
    this._navController.push(TeamSelectPage, {
      tournament: this.tournament
    });
  }
}
