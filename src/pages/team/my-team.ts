import {Component} from '@angular/core';
import {NavController, NavParams, Refresher} from 'ionic-angular';
import {TeamService} from '../../providers/team-service';
import {IPool, ITeam, ITournament} from '../../models/models';
import {Observable} from 'rxjs/Observable';
import {TeamSelectPage} from '../team-select/team-select';
import {PoolPage} from '../pool/pool';
import {MatchPage} from '../match/match';

/**
 * MyTeamPage Page Component extends TeamPage to allow for specific functionality based on the users selected team
 * context.
 */
@Component({
  selector: 'team',
  templateUrl: 'my-team.html'
})
export class MyTeamPage {

  private teamNotFound: boolean;
  private team: Observable<ITeam>;
  private pool: Observable<IPool>;
  private localPool: IPool;
  private tournament: ITournament;

  constructor(private _navParams: NavParams,
              private _navController: NavController,
              private _teamService: TeamService) {
    this.tournament = this._navParams.get('tournament');
    this.teamNotFound = this._navParams.get('teamNotFound') || false;

    this.team = this._teamService.getCurrentTeamSubject
      .do((team: ITeam) => {
        this.teamNotFound = !team
      }); // Listen for any changes on the current team

    this.pool = this._teamService.getCurrentTeamSubject
      .switchMap((team: ITeam) => {
        return team ? this._teamService.getTeamPool(team._id) : Observable.of(null);
      })
      .do((pool: IPool) => this.localPool = pool);
  }

  /**
   * Sync the team
   */
  public syncTeam() {
    return this._teamService
      .sync(this.tournament._id)
      .first();
  }

  /**
   * Reload team data from pull to refresh.
   * @param {Refresher} refresher
   */
  public reloadTeam(refresher: Refresher) {
    this.syncTeam()
      .subscribe(
        () => refresher.complete(),
        () => refresher.cancel()
      );
  }

  /**
   * Sets the current local team, and changes the toggle ui
   * @param {ITeam} team
   */
  setTeam(team: ITeam) {
    this._teamService.setTeam(team, this.tournament._id).first().subscribe();
  }

  /**
   * Removes the current local team from storage, and change the toggle ui
   */
  removeTeam(team: ITeam) {
    this._teamService.removeTeam(team, this.tournament._id).first().subscribe();
  }

  /**
   * Select a match and navigate to the view
   * @param {string} matchId
   */
  matchSelected(matchId: string) {
    this._navController.push(MatchPage, {
      matchId: matchId,
      pool: this.localPool
    });
  }

  /**
   * Select a pool and navigate to the view
   * @param {string} poolId
   */
  poolSelected(poolId: string) {
    this._navController.push(PoolPage, {
      poolId: poolId,
      tournament: this.tournament
    });
  }

  /**
   * Callback for selectTeam button on team-not-found page
   */
  selectTeam() {
    this._navController.push(TeamSelectPage, {
      tournament: this.tournament
    });
  }

}
