import {Component} from '@angular/core';
import {UserService} from "../../providers/user-service";
import {ITeam, ITournament, IUser} from "../../models/models";
import {Observable} from "rxjs/Observable";
import {GlobalEventService, GlobalEventTopics} from "../../providers/global-event-service";
import {TournamentService} from "../../providers/tournament-service";
import {TeamSelectPage} from "../team-select/team-select";
import {TabsPage} from "../tabs/tabs";
import {TeamService} from "../../providers/team-service";
import {NavController} from "ionic-angular";
import {NavigationHelpers} from "../../helpers/navigation-helpers";

@Component({
  selector: 'account',
  templateUrl: 'account.html',
})
export class AccountPage {

  private _user: Observable<IUser>;
  private _tournaments: Observable<ITournament[]>;

  constructor(private _userService: UserService,
              private _globalEventService: GlobalEventService,
              private _navController: NavController,
              private _navigationHelpers: NavigationHelpers,
              private _teamService: TeamService,
              private _tournamentService: TournamentService) {
    this._user = this._userService.getUser();
    this._tournaments = this._tournamentService.getTournamentsAndTeamsForCurrentUser();
  }

  /**
   * Select a tournament a head to the teams page for those in the tournament
   */
  tournamentSelected(tournament: ITournament) {
    this._navigationHelpers.navigateToTournament(tournament);
  }

  /**
   * Log out
   */
  public logout() {
    this._globalEventService.publish(GlobalEventTopics.UserLogout);
  }
}
