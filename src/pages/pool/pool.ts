import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {DataService} from '../../providers/data-service';
import {IMatch, IPool, ITeam, ITournament} from '../../models/models';
import * as _ from 'lodash';
import {MatchPage} from '../match/match';
import {TeamPage} from "../team/team";
import {getMatchFormat, getMatchLoser, getMatchRecordString, getMatchWinner} from '../../lib/match';


@Component({
  selector: 'page-pool',
  templateUrl: 'pool.html',
})
export class PoolPage {

  public pool: IPool;
  public tournament: ITournament;
  public matchFormat: string;
  public matchResults: { [team: string]: number };
  public resultItemsCollapsed: boolean = true;
  public resultsIcon: string = 'arrow-up';


  constructor(private _dataService: DataService,
              private _navController: NavController,
              private _loadingController: LoadingController,
              private _navParams: NavParams) {
    this.tournament = this._navParams.get('tournament');
  }

  ionViewWillEnter() {
    let poolId = this._navParams.get('poolId');
    let loader = this._loadingController.create();
    loader.present();
    this._dataService.getPool(poolId).subscribe(
      data => {
        this._instantiateViewModel(data)
      },
      err => {
        console.log('Failed to get pool.');
        loader.dismiss();
      },
      () => {
        loader.dismiss();
      }
    );
  }

  /**
   * Instantiate the view model.
   * @param {IPool} pool
   * @private
   */
  private _instantiateViewModel(pool: IPool) {
    this.matchFormat = getMatchFormat(pool.format.gameScores);
    pool.matches = _.map(pool.matches, match => {
      match.gameRecord = getMatchRecordString(match);
      return match;
    });
    this.matchResults = this._getMatchResults(pool);
    // Sort the teams by the best record
    pool.teams = _.sortBy(pool.teams, [(team: ITeam) => -_.get(this.matchResults[team._id], 'wins', 0)]);
    this.pool = pool;
  }

  /**
   * Navigate to Match Page
   * @param match {IMatch}
   */
  public selectMatch(match) {
    this._navController.push(MatchPage, {
      matchId: match['_id'],
      tournament: this.tournament,
      pool: this.pool
    });
  }

  /**
   * Select a tournament a head to the teams page for those in the tournament
   */
  public teamSelected(team: ITeam) {
    this._navController.push(TeamPage, {
      teamId: team._id,
      tournament: this.tournament
    });
  }

  /**
   * Toggle results list.
   */
  public toggleResultsList() {
    this.resultItemsCollapsed = !this.resultItemsCollapsed;
    this.resultsIcon = this.resultItemsCollapsed ? 'arrow-up' : 'arrow-down';
  }

  /**
   * Get match results to display.
   * @param {IPool} pool
   * @return {}
   * @private
   */
  private _getMatchResults(pool: IPool) {
    return _.reduce(pool.matches, (result, match: IMatch) => {

      _.each(match.teams, (team: { _id: string, title: string }) => {
        if (!result[team._id]) result[team._id] = {wins: 0, losses: 0}
      });

      let winner = getMatchWinner(match);
      let loser = getMatchLoser(match);

      if (!winner || !loser) {
        return result;
      }

      result[winner].wins += 1;
      result[loser].losses += 1;

      return result;
    }, {});
  }
}
