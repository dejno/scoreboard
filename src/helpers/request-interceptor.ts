import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ENV} from '@app/env';
import {GlobalEventService, GlobalEventTopics} from "../providers/global-event-service";
import {TokenHelper} from "../providers/user-service";

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private _tokenHelper: TokenHelper,
              private _globalEventService: GlobalEventService) {
  }

  /**
   * Better way to handle auth + generating URL from env
   * @param {HttpRequest<any>} request
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<any>>}
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this._tokenHelper.getToken().flatMap((token) => {
      request = request.clone({
        withCredentials: true,
        headers: request.headers
          .append('Authorization', 'Bearer ' + token)
          .append('Content-Type', 'application/json'),
        url: this._generateUrlFromEnv(request.url),
      });
      return next.handle(request).catch(response => {
        if (response instanceof HttpErrorResponse) {
          if (response.status === 401) {
            this._globalEventService.publish(GlobalEventTopics.UserLogout);
          }
        }

        return Observable.throw(response);
      });
    });
  }

  /**
   * Generates a url with the correct API
   * @param {string} url
   * @returns {string}
   */
  private _generateUrlFromEnv(url: string): string {
    return ENV.mode === 'prod' ? ENV.api + url : url;
  }
}


