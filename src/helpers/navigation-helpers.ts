import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ITeam, ITournament} from "../models/models";
import {TabsPage} from "../pages/tabs/tabs";
import {TeamService} from "../providers/team-service";
import {TournamentService} from "../providers/tournament-service";
import {App, NavController} from "ionic-angular";
import {TeamSelectPage} from "../pages/team-select/team-select";

@Injectable()
export class NavigationHelpers {

  constructor(private _app: App,
              private _teamService: TeamService,
              private _tournamentService: TournamentService) {
  }

  /**
   * Getter for the NavController because we can't inject it into the service
   * @returns {NavController}
   */
  get navigationController(): NavController {
    let rootNavs = this._app.getRootNavs() || [];
    return rootNavs[0];
  }

  /**
   * Encapsulates the logic needed for navigating to a tournament
   * @param {ITournament} tournament
   */
  public navigateToTournament(tournament: ITournament) {
    this._tournamentService.setLocalTournament(tournament)
      .flatMap(() => this._teamService.getCurrentUserTeamByTournament(tournament._id))
      .catch(() => Observable.of(null)) // Handle when the team is not found
      .flatMap((team: ITeam) => this._teamService.setLocalTeam(team, tournament._id))
      .subscribe((team: ITeam) => {
        this.navigationController.setRoot(team ? TabsPage : TeamSelectPage, {
          tournament: tournament,
          team: team
        }, {animate: true, direction: 'forward'});
      });
  }
}


