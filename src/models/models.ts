/**
 * Data Models
 */

export interface IUser {
  _id: string,
  name: any,
  displayName: string,
  firstName: string,
  lastName: string,
  email: string,
  image: string,
  gender: string,
  sports: Array<string>,
  facebookInfo: any,
  memberships: Array<any>
}

export interface ITournament {
  _id: string
  title: string
  organizers?: Array<{
    name: string
    company?: string
    email?: string
    phoneNumber?: string
    website?: string
  }>
  dates: Array<Date>
  location: {
    name: string
    city: string
    state: string
    address: string
  }
  sport: "Volleyball"
  divisions: Array<IDivision>,
  image: string,
  teams?: ITeam[],
  creator?: IUser,
  status: string
}


export interface IDivision {
  _id: string
  level: string
  gender: "Men" | "Women" | "Coed"
  teams?: Array<ITeam>
  pools?: Array<IPool>
}

export interface IPool {
  _id: string,
  title: string,
  startTime: string,
  division: string,
  teams: Array<ITeam | string>,
  location?: { location?: string, court?: string },
  format?: { gameScores?: Array<{ target: number, cap: number, _id: string }> }
  matches?: Array<string | IMatch>
}


export interface ITeam {
  _id: string
  title: string
  members?: Array<IUser>
  tournaments?: Array<string>,
  isTeam?: boolean,
  metadata: any
}

export interface IMatch {
  _id: string
  teams: Array<{ _id: string, title: string }>
  refTeam: { _id: string, title: string }
  scheduledTime?: Date
  startTime?: Date
  location?: {
    court?: string
  }
  games?: Array<IGame>

}

export interface IGame {
  _id: string
  final: boolean
  scores?: Array<IScores>
}

export interface IScores {
  timestamp?: Date
  score: Array<{
    team: string
    points: number
  }>
}


/**
 * View Models
 */

export interface ITimeGroupListView {
  present: {
    listItems: Array<ITournament>,
    title: string
  },
  future: {
    listItems: Array<ITournament>,
    title: string
  },
  past: {
    listItems: Array<ITournament>,
    title: string
  },
}
