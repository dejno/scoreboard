import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';

@Pipe({name: 'keys', pure: false})
export class KeysPipe implements PipeTransform {
  transform(value: any, args: any[] = null): any {
    return Object.keys(value)
  }
}

/**
 * Given an array of dates, return a string representing a date range.
 * @example: [Date('2017-11-1'), Date('2017-11-3')] yields 'Nov 1-3, 2017'
 * @example: [Date('2017-11-1')] yields Nov 1, 2017
 */
@Pipe({name: 'dateRangeDisplay', pure: true})
export class DateRangeDisplayPipe extends DatePipe implements PipeTransform {
  transform(dates: Array<Date>, ...args: any[]): string | null {
    if (!dates.length)
      return;
    dates.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
    let pattern = new Date(dates[0]).getMonth() === new Date(dates[dates.length - 1]).getMonth() ? 'd, yyyy' : 'MMM d, yyyy';
    return (dates.length > 1)
      ? super.transform(dates[0], 'MMM d') + ' - ' + super.transform(dates[dates.length - 1], pattern)
      : super.transform(dates[0]);
  }
}

/**
 *Given an array of strings, construct a delimited string using the delim param
 * @note delimiter will not add space between pieces unless included.
 * @example
 *      {{
 *       [
 *       (match.scheduledTime | date:'ha'),
 *       (match.location ? match.location.court : null),
 *       (match.refTeam ? match.refTeam.title : null)
 *       ] | delim}}
 * yields Date | court | title
 * if only one defined: will not have delimiter.
 */
@Pipe({name: 'delim', pure: true})
export class DelimPipe implements PipeTransform {
  transform(pieces: string[], delim: string = ' | ', ...args: any[]): any {
    let result = '';
    _.each(_.without(pieces, undefined, '', null, NaN, false), (piece, i) => {
        result += i === 0 ? piece : delim + piece;
    });
    return result;
  }
}

/**
 * Get value in object at path. Return default value if provided and path does not have a value.
 * @return {any} value at path in object or if falsey, the default.
 */
@Pipe({name: 'get', pure: true})
export class GetPipe implements PipeTransform {
  transform(object: Object | Array<any>, path: string, defaultValue?: any, ...args: any[]) {
    return _.get(object, path, defaultValue)  || '';
  }
}
