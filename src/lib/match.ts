import {IMatch} from '../models/models';
import _ from 'lodash';

/**
 * Get current match record based on scores and final flag.
 * @param {IMatch} match
 * @return {{[teamId: string]: { wins: number, losses: number}}}
 */
export function getMatchRecord(match: IMatch) {
  let games = _.filter(match.games, 'final');

  if (!games.length) {
    return {};
  }

  let keyMap = _.keyBy(match.teams, (team: { _id: string, title: string }) => team._id);
  keyMap = _.mapValues(keyMap, () => ({wins: 0, losses: 0}));

  _.each(games, game => {
    let final = _.last(game.scores);
    let gameWinner = _.get(_.maxBy(final.score, 'points', 0), 'team');
    let gameLoser = _.get(_.minBy(final.score, 'points', 0), 'team');
    keyMap[gameLoser].losses += 1;
    keyMap[gameWinner].wins += 1;
  });

  return keyMap;
}

/**
 * Get the winner of the match IFF number of games finished is greater than half of total games.
 * @param {IMatch} match
 * @return {string}
 */
export function getMatchWinner(match: IMatch): string {
  if (parseInt(match.games.length / 2 + '') + 1 > _.filter(match.games, game => game.final).length) {  //if all games are finished
    return '';
  }

  let record = getMatchRecord(match);
  return _.keys(record).reduce((a, b) => {
    return record[a].wins > record[b].wins ? a : b
  });
}

/**
 * Get the winner of the match IFF number of games finished is greater than half of total games.
 * @param {IMatch} match
 * @return {string}
 */
export function getMatchLoser(match: IMatch): string {
  if (parseInt(match.games.length / 2 + '') + 1 > _.filter(match.games, game => game.final).length) {  //if all games are finished
    return '';
  }

  let record = getMatchRecord(match);
  return _.keys(record).reduce((a, b) => {
    return record[a].losses > record[b].losses ? a : b
  });
}

/**
 * Get a the match record string.
 * @example '1 - 2'
 * @param {IMatch} match
 * @param {string} delim
 * @return {string}
 */
export function getMatchRecordString(match: IMatch, delim: string = '-'): string {
  if (!match || !match.teams) {
    return ''
  }

  let record = getMatchRecord(match);
  return `${_.get(record[match.teams[0]._id], 'wins', 0)} ${delim} ${_.get(record[match.teams[0]._id], 'losses', 0)}`
}

/**
 * Get a human readable string for the match formats.
 * @param {Array<number>} gameScores
 * @returns {string}
 * @TODO: Needs to be more robust for varying game scores, but find for now.
 */
export function getMatchFormat(gameScores: Array<{ target: number, cap: number }> | null): string {
  if (!gameScores || gameScores.length === 0) return '';

  let format = `${gameScores.length} Game${gameScores.length > 1 ? 's' : ''} to ${gameScores[0].target}`;
  if (_.last(gameScores) !== gameScores[0]) {
    const gameMap = {
      2: 'second',
      3: 'third',
      4: 'forth',
      5: 'fifth'
    };

    format += `, ${gameMap[gameScores.length]} to ${_.get(_.last(gameScores), 'target')}`
  }
  return format
}


