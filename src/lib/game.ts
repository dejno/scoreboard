import _ from 'lodash';
import {IGame} from '../models/models';

declare interface ITeamScoreViewModel {
  team: string
  title: string
  points: number
}


//#region exports
/**
 * Interface to use for displaying game scores with team names.
 */
export interface ITeamScoreViewModelMap {
  [teamId: string]: ITeamScoreViewModel
}

/**
 * Given a IGame and Array<ITeam> build  ITeamScoreViewModel object
 * @param {IGame} game
 * @param {Array<ITeam>} teams
 * @returns {ITeamScoreViewModelMap}
 */
export function getTeamScoreViewModelMapFromGame(game: IGame, teams: Array<{ _id: string, title: string }>): ITeamScoreViewModelMap {
  return _
    .chain(game.scores)
    .last()
    .defaultsDeep({
        timestamp: Date.now(),
        score: _.map(teams, team => {
          return {team: team['_id'], points: 0}
        })
      }
    )
    .get('score')
    .map(score => _.set(score, 'title', _.get(teams.find(team => team['_id'] === score['team']), 'title')))
    .mapKeys(val => val.team)
    .value();

}

/**
 * Get the winner of the game based on points.
 * @param {ITeamScoreViewModelMap} scores
 * @return {string} - team id of winner. '' if tied
 */
export function getGameWinner(scores: ITeamScoreViewModelMap): string {
  let teams = _.values(scores);
  return teams[0].points > teams[1].points ? teams[0].team : teams[1].points > teams[0].points ? teams[1].team : '';
}

//#endregion
