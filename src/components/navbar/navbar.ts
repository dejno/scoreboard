import {Component, Input} from '@angular/core';

/**
 * Generated class for the NavbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector   : 'scoreboard-navbar',
  templateUrl: 'navbar.html'
})
export class NavbarComponent {

  @Input() title: string;

  constructor() {}

}
