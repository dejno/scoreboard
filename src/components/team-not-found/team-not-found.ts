import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'team-not-found',
  templateUrl: 'team-not-found.html'
})
export class TeamNotFoundComponent {

  @Output() selectTeam = new EventEmitter();

  constructor() {
  }

  navigateToTournamentPage() {
    this.selectTeam.emit();
  }
}
