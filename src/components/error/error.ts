import {Component, EventEmitter, Output} from "@angular/core";

@Component({
  selector: 'error',
  templateUrl: 'error.html'
})
export class ErrorComponent {

  @Output() refresh: EventEmitter<any> = new EventEmitter();

  constructor() {}

  public refreshHandler() {
    this.refresh.emit();
  }
}
