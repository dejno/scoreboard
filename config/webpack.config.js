var chalk = require('chalk');
var fs = require('fs');
var path = require('path');
var useDefaultConfig = require('@ionic/app-scripts/config/webpack.config.js');

function environmentPath() {
  var env = process.env.IONIC_ENV;
  var filePath = './src/environments/environment' + (env === 'prod' ? '' : '.' + env) + '.ts';
  if (!fs.existsSync(filePath)) {
    console.log(chalk.red('\n' + filePath + ' does not exist!'));
  } else {
    console.log(chalk.green('\n' + filePath + ' does exist!'));
    return filePath;
  }
}

module.exports = function () {
  var env = process.env.IONIC_ENV;
  var envPath = path.resolve(environmentPath());
  useDefaultConfig[env].resolve.alias = {
    '@app/env': envPath
  };
  return useDefaultConfig;
};
